from django.http import Http404
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.views.generic.edit import FormView
from django.views.generic import View
from testgui.forms import ArticleForm
from testgui.models import Article
from testgui.utils import get_factors_from_tlc_data


class ArticleFormView(View):
    def get(self, request):
        articles_buzzfeed = Article.objects.filter(source="buzzfeed")
        articles_mashable = Article.objects.filter(source="mashable")
        articles_techcrunch = Article.objects.filter(source="techcrunch")
        context = {
            "form": ArticleForm,
            "articles_buzzfeed": articles_buzzfeed,
            "articles_mashable": articles_mashable,
            "articles_techcrunch": articles_techcrunch
        }
        return render(request, "article_form.html", context)

    def post(self, request):
        form = ArticleForm(request.POST)
        if form.is_valid():
            article = get_factors_from_tlc_data(form.cleaned_data["lead"], form.cleaned_data["title"],
                                                form.cleaned_data["content"], form.cleaned_data["source"])
            article_obj = Article.objects.create(lead=article["lead"], title=article["title"],
                                                 content=article["content"], source=article["source"])
            return redirect(reverse("article", kwargs={"article_id": article_obj.id}))
        raise Http404


class ArticleView(View):
    def get(self, request, article_id):
        context = {
            "article": Article.objects.get(id=article_id)
        }
        return render(request, "article.html", context)
