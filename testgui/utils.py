
def get_factors_from_tlc_data(lead, title, content, source):
    # input data must be in the next order: Lead, Title, Content

    pub_ltc_dict = {
        'lead': lead,
        'title': title,
        'content': content,
        'source': source
    }

    return pub_ltc_dict
