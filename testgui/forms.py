from django.forms import Form, CharField, Textarea


class ArticleForm(Form):
    title = CharField(label='Title', max_length=100)
    lead = CharField(label='Lead', max_length=100)
    content = CharField(label='Content', widget=Textarea(attrs={'rows': 4, 'cols': 40}))
    source = CharField(label='Source', max_length=15)
