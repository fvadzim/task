from django.db import models


class Article(models.Model):
    title = models.CharField(max_length=256)
    lead = models.CharField(max_length=256)
    content = models.TextField()
    source = models.CharField(max_length=15)
